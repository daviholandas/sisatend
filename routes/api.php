<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('API')-> name('api.')->group(function(){
    Route::prefix('empresas')->group(function(){
        Route::get('/','EmpresaController@index') ->name('empresas');
        Route::get('/{nomeEmpresa}','EmpresaController@getEmpresa');
    });
    Route::prefix('riscos')->group(function(){
        Route::get('/{nomeRiscos}','RiscoController@getRiscos')->name('riscos'); 
    }); 
    Route::prefix('valores')->group(function(){
        Route::get('/{codigoEmpresa}','EmpresaController@empresasValores');
    });
    Route::prefix('atendimentos')->group(function(){
        Route::get('/','AtendimentoController@allAtendimentos');
        Route::get('/info/{dados}','AtendimentoController@getAtendimento');
        Route::get('/aso/{numeroColaborador}','AtendimentoController@getAso');
        Route::get('/foto/{idColaborador}','AtendimentoController@getPhotoColaborador');
    });
});