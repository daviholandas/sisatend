<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Empresa;
use App\Services\EmpresaService;

class EmpresaController extends Controller
{

    function __construct(EmpresaService $empresaService){
        $this->empresaService = $empresaService;
    }
   
    public function index(){
       $empresas = $this -> empresaService -> getEmpresas();
       return response()->json($empresas);
   }

   public function getEmpresa($nomeEmpresa){
        $empresas = $this-> empresaService -> findEmpresa($nomeEmpresa);
        return response()->json($empresas);
   }

   public function empresasValores($codigoEmpresa){
       $empresas = $this -> empresaService -> getValoresEmpresas($codigoEmpresa);
       return response() -> json($empresas);
   }
}
