<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RiscoService;

class RiscoController extends Controller
{

    function __construct(RiscoService $riscoService){
        $this->riscoService = $riscoService;
    }


    public function getRiscos($codigoRisco){
        $riscos = $this->riscoService->findRiscos($codigoRisco);
        return response()->json($riscos);
    }
}
