<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AtendimentoService;
use App\Models\Atendimento;

class AtendimentoController extends Controller
{
    public function __construct(AtendimentoService $atendimentoService){
        $this->atendimentoService = $atendimentoService;
    }

    public function getAtendimento($dados){
        $atendimento =  $this->atendimentoService->findAtendimento($dados);
        return response()->json($atendimento);
    }

    public function getAso($numeroColaborador){
        $aso = $this->atendimentoService->findAso($numeroColaborador);
        return response($aso) -> header('Content-type','image/jpg');
    }

    public function allAtendimentos(){
        $atendimentos = $this->atendimentoService->findAtendimentos();
        return response()->json($atendimentos);
    }

    public function getPhotoColaborador($idColaborador){
        $photo = $this->atendimentoService->findPhoto($idColaborador);
        return response($photo)->header('Content-type','image/jpg');
    }
}
