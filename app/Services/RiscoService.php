<?php

namespace App\Services;

use App\Models\Risco;

Class RiscoService
{
    private $_riscoModel;

    function __construct(Risco $riscoModel){
        $this->_riscoModel = $riscoModel;
    }

    public function findRiscos($codigoRisco){
        return $this-> _riscoModel->select('NOME_FUNCAO','FISICOS','QUIMICOS','BIOLOGICOS','ERGONOMICOS','ACIDENTES','OUTROS')->where('COD_RISCOS', $codigoRisco)->get();
    }
}