<?php

namespace App\Services;

use Storage;
use App\Models\Atendimento;

class AtendimentoService
{
    private $_atendimentoModel;

    function __construct(Atendimento $atendimentoModel){
        $this -> _atendimentoModel = $atendimentoModel;
    }

    public function findAtendimentos(){
        return $this->_atendimentoModel->select(
            'CODIGO','N_FICHA','DATA','TIPO_ATEND','NOME_FUN','RAZAO_EMP','CNPJ','COD_FUN','NOME_FUN','DT_NASC_FUN','IDADE_FUN','FONE_FUN',
            'SEXO','COD_FUNCAO','DESC_FUNCAO','T_EXAME','TEMP_SERV'
        )->paginate(20);
        
    }

    public function findAtendimento($dados){
        return $this->_atendimentoModel->select(
            'CODIGO','N_FICHA','DATA','TIPO_ATEND','RAZAO_EMP','CNPJ','COD_FUN','NOME_FUN','DT_NASC_FUN','IDADE_FUN','FONE_FUN',
            'SEXO','COD_FUNCAO','DESC_FUNCAO','T_EXAME','TEMP_SERV'
        )->where('NOME_FUN','CONTAINING',$dados)->orWhere('RAZAO_EMP','CONTAINING',$dados)->paginate(20);
        
    }

    public function findAso($numeroColaborador){
        
        return  Storage::disk('aso')-> get("{$numeroColaborador}.jpg");
        
    }

    public function findPhoto($idColaborador){
        return Storage::disk('foto')->get("{$idColaborador}.jpg");
    }
}