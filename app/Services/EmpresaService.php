<?php

namespace App\Services;

use App\Models\Empresa;
use App\Models\Valores;

class  EmpresaService 
{
   private $_empresaModel;
   private $_valoresModel;

   function __construct(Empresa $empresaModel, Valores $valoresModel){
       $this -> _empresaModel = $empresaModel;
       $this -> _valoresModel = $valoresModel;
   }

   public function getEmpresas(){
        return $this->_empresaModel -> paginate(20);
   }

   public function getValoresEmpresas($codigoEmpresa){
        /* return $this -> _empresaModel -> join('VALORES', 'EMPRESAS.NOME', '=', 'VALORES.NOMEEMPRESA'); */
        return $this->_valoresModel -> select('NOMEEMPRESA','NOMEEXAME', 'VALOR') -> where('CODEMPRESA',$codigoEmpresa)->get();
   }

   public function findEmpresa($nomeEmpresa){
        return $this->_empresaModel -> select() -> where('NOME', 'CONTAINING',$nomeEmpresa)-> orWhere('CNPJ','CONTAINING',$nomeEmpresa)-> get();
   }

}